class ProjectFinder < BaseFinder
  def initialize(path: Config::GROUP_PROJECT_PATH, date: nil, branch_name: nil, file_path: [])
    @query_class = ::GraphQL::Queries::ProjectQuery

    @variables = {
      path: path,
      date: date,
      branchName: branch_name,
      filePath: file_path
    }
  end

  def self.date(date)
    scope_variables[:date] = date

    return self
  end

  def self.file(file_path)
    scope_variables[:branchName] = Config::DEFAULT_REPO_BRANCH
    scope_variables[:filePath] = file_path

    return self
  end
end