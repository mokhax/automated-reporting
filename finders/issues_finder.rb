class IssuesFinder < BaseFinder
  extend IssuableScopes

  SCOPE_VARIABLES_DEFAULT = { labelNames: [] }

  def initialize(state: nil, label_names: [], closed_after: nil, group_label_name: nil, epic_id: nil, search: nil, search_in: [])
    @query_class = ::GraphQL::Queries::IssuesQuery

    @variables = {
      state: state,
      groupLabelName: group_label_name,
      labelNames: label_names,
      closedAfter: closed_after,
      epicId: epic_id,
      search: search,
      in: search_in
    }
  end
end
