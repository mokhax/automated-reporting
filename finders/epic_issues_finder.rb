class EpicIssuesFinder < BaseFinder
  extend IssuableScopes

  SCOPE_VARIABLES_DEFAULT = { labelNames: [] }

  def initialize(label_names: nil)
    @query_class = ::GraphQL::Queries::EpicIssuesQuery

    @variables = {
      labelNames: [label_names]
    }
  end

  def self.for_group
    # Epic issue "or" filtering is broken in the Epic GraphQL API,
    # so we're passing labels together here instead of using groupLabelName
    self.scope_variables[:labelNames].push(Config::GROUP_LABEL)

    return self
  end
end