class BaseFinder
  attr_accessor :label_names, :query_class, :variables

  SCOPE_VARIABLES_DEFAULT = {}

  def self.call(args = {})
    instance = new(**args)

    variables = instance.variables.deep_merge(self.scope_variables)
    nodes = instance.query_class.new(variables: variables).nodes

    self.clear_scope_variables!

    return nodes
  end

  def self.clear_scope_variables!
    @@scope_variables = nil
  end

  def self.scope_variables
    return @@scope_variables if self.class_variable_defined?(:@@scope_variables) && !!@@scope_variables

    @@scope_variables = Marshal.load(Marshal.dump(self::SCOPE_VARIABLES_DEFAULT))
  end
end