require './app'

namespace :okr do
  desc "Generate a markdown report including issues and epics that have the OKR_LABEL and GROUP_LABEL"
  task :report do
    presenter = OKRReportPresenter.call

    OKRReportTemplate.render(presenter)
  end
end
