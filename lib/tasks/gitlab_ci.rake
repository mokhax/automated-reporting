require './app'

namespace :gitlab_ci do
  task :generate do
    tasks = with_captured_stdout do
      puts %x{rake -P}
    end

    task_list = tasks.split.reject { |task| ['rake', 'gitlab_ci:generate'].any?(task) }

    input_hash = process_commands(task_list)

    yaml_output = hash_to_yaml(input_hash)

    data_path = File.join(Dir.pwd, ".gitlab-ci.yml")
    File.write(data_path, yaml_output)
    puts "Data written to #{data_path}"
  end
end

def process_commands(commands)
  result = {}
  current_namespace = result
  for command in commands
    # Split the command by colons to get namespace and task (or sub-namespace and sub-task)
    parts = command.split(":")
    current_namespace = result

    # Iterate through namespace parts, creating sub-hashes as needed
    parts.each_with_index do |part, index|
      # If it's the last part, set the task value to true
      if index == parts.length - 1
        current_namespace[part] = true
      else
        # Create a sub-hash for the namespace part
        current_namespace = current_namespace[part] ||= {}
      end
    end
  end
  return result
end

def hash_to_yaml(hash)
  jobs = {}
  stages = []
  build_jobs_and_stages(hash, jobs, stages, [])

  { "stages" => stages.uniq, **jobs }.to_yaml
end

def build_jobs_and_stages(hash, jobs, stages, key_prefix)
  hash.each do |key, value|
    new_key_prefix = key_prefix + [key.to_s]
    if value.is_a?(Hash)
      build_jobs_and_stages(value, jobs, stages, new_key_prefix)
    else
      job_name = new_key_prefix.join('-').gsub('_', '-') + '-job'
      stage_name = new_key_prefix.join('-').gsub('_', '-')
      variable_name = new_key_prefix.map(&:upcase).join('_') + '_JOB'
      jobs[job_name] = {
        'stage' => stage_name,
        'script' => [
          'bundle install',
          "bundle exec rake #{new_key_prefix.join(':')}"
        ],
        'only' => {
          'variables' => ["($#{variable_name} == \"true\")"]
        }
      }
      stages << stage_name
    end
  end
end
