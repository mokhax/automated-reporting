require './app'

namespace :engineers do
  desc "Generate a markdown report of GROUP_ENGINEERS, including their current MR Rate, MTTM and closed MR links"
  task :report do
    presenter = EngineersReportPresenter.call

    EngineersReportTemplate.render(presenter)
  end

  namespace :issues do
    desc 'Close all issues that have the ENGINEERS_REPORT_TITLE_STRING, that are in the ENGINEERS_REPORT_EPIC_ID.'
    task :close_all do
      issues = IssuesFinder.epic(Config::ENGINEERS_REPORT_EPIC_ID).
        title(Config::ENGINEERS_REPORT_TITLE_STRING).open.call

      UpdateIssueService.call(
        parentId: ProjectFinder.call.id,
        ids: issues.map(&:id),
        stateEvent: 'CLOSE'
      )
    end

    desc 'Runs rake:engineers:issues:close_all and posts content from rake:engineers:report in new issue under ENGINEERS_REPOR_EPIC_ID'
    task :create do
      Rake::Task["engineers:issues:close_all"].invoke

      body = with_captured_stdout do
        Rake::Task["engineers:report"].invoke
      end

      CreateIssueService.call(
        title: "#{ Config::ENGINEERS_REPORT_TITLE_STRING} - #{ Date.today.strftime('%Y-%m-%d') }",
        description: body,
        epic_id: Config::ENGINEERS_REPORT_EPIC_GLOBAL_ID
      )
    end
  end
end