require './app'

namespace :weekly_update do
  desc "Generate a markdown report including issues and epics that have the GROUP_LABEL and a AUTO_REPORT_LABEL, SECURITY_LABEL or INFRADEV_LABEL"
  task :report do
    presenter = WeeklyReportPresenter.call

    WeeklyReportTemplate.render(presenter)
  end

  desc 'Adds a note to the newest WEEKLY_UPDATE_TITLE_SEARCH_STRING issue found in the WEEKLY_UPDATE_EPIC_ID with the weekly_update:report output'
  task :comment do
    weekly_report_issues = IssuesFinder.open.weekly_update.call

    newest_issue = weekly_report_issues.sort_by{|i| i.created_at }.last

    body = with_captured_stdout do
      Rake::Task["weekly_update:report"].invoke
    end

    CreateDiscussionService.call(
      body: body,
      noteableId: newest_issue.id
    )
  end
end