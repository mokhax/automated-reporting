require './app'

namespace :planning do
  namespace :issues do
    desc 'Close all issues that have the GROUP_LABEL and PLANNING_LABEL, that belong to the project_gid arg.'
    task :close_all, [:project_gid] do |t, args|
      planning_issues = IssuesFinder.for_group.planning.open.call

      UpdateIssueService.call(
        parentId: args.project_gid,
        ids: planning_issues.map(&:id),
        stateEvent: 'CLOSE'
      )
    end

    desc 'Runs planning:issues:close_all, and creates a new planning issue in the GROUP_PROJECT_PATH using the PLANNING_TEMPLATE_PATH template'
    task :create do
      ## Milestone starts on saturday before 3rd thursday.
      ## If scheduling a job, set to run on 1st thursday of month.
      DATE_CONTAINED_IN_NEXT_MILESTONE = 2.weeks.from_now.iso8601

      presenter = Issues::PlanningIssuePresenter.new(date: DATE_CONTAINED_IN_NEXT_MILESTONE)

      Rake::Task["planning:issues:close_all"].invoke(presenter.project_gid)

      CreateIssueService.call(
        title: presenter.title,
        description: presenter.description
      )
    end
  end
end