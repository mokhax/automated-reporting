class OKRReportPresenter
  def self.call
    OpenStruct.new({
      issues: IssuesFinder.for_group.okr.call,
      epic_issues: EpicIssuesFinder.for_group.okr.call
    })
  end
end