class WeeklyReportPresenter
  def self.call
    context = OpenStruct.new({
      open_issues: IssuesFinder.for_group.auto_reporting.open.call,
      closed_issues: IssuesFinder.for_group.auto_reporting.closed_after(1.week.ago.iso8601).call,
      epic_issues: EpicIssuesFinder.for_group.auto_reporting.call,
      security_issues: IssuesFinder.for_group.security.open.call,
      infradev_issues: IssuesFinder.for_group.infradev.open.call
    })
  end
end