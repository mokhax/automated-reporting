require_relative 'issuable_presenter'

class IssuePresenter
  include IssuablePresenter

  def discussions
    return object.discussions if object.respond_to?(:discussions)

    nil
  end
end
