module IssuablePresenter
  attr_accessor :object

  def initialize(object:)
    @object = object
  end

  def health_status
    status = object.health_status

    return '-' if status.to_s.empty?

    "**#{ status.underscore.humanize }**"
  end

  def dri_comment
    return if discussions.nil?

    ## BUG - this dupes the push
    issuable_dris = dris.push(Config::USERNAME)

    notes = discussions.nodes.map(&:notes).map(&:nodes).flatten

    return if notes.empty?

    dri_notes = notes.filter do |note|
      Date.parse(note.created_at) > 1.week.ago &&
      issuable_dris.any?(note.author.username) &&
      note.award_emoji.nodes.map(&:name).any?(Config::DRI_UPDATE_EMOJI)
    end

    return if dri_notes.empty?

    dri_note = dri_notes.sort_by{|i| i.created_at }.last

    dri_note.body.delete('@').gsub("\n", ' ')
  end

  def labels(regex)
    matches = object.labels.nodes.map(&:title).select {|label| label =~ regex }
    return [] if matches.empty?

    matches.map {|label| "~\"#{label}\"" }
  end

  def all_labels
    markdown = object.labels.nodes.map do |label|
      "~\"#{label.title}\""
    end
    markdown.join(' ')
  end

  def assignees
    assignees = object.assignees

    return "Unassigned" if assignees.nodes.empty?

    assignees.nodes.map(&:username).join(", ")
  end

  def method_missing(m, *args, &block)
    object.send(m, *args, &block)
  end

  private

  def dris
    object.assignees.nodes.map(&:username)
  end
end
