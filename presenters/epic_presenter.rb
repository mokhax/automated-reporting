require_relative 'issuable_presenter'

class EpicPresenter
  include IssueSortable
  include IssuablePresenter

  def issues
    nodes = object.issues.nodes.map do |issue|
      IssuePresenter.new(object: issue)
    end
    sort_by_workflow_and_state(nodes)
  end

  def progress
    states = issues.map(&:state)
    tally = states.tally

    "#{ tally["closed"] || 0 } / #{ states.count } Closed"
  end

  private

  def dris
    Config::GROUP_ENGINEERS
  end
end
