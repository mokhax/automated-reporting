class EngineersReportPresenter
  def self.call
    @end_date = Date.tomorrow
    @start_date = @end_date - 1.month
    context = OpenStruct.new({
      start_date: @start_date,
      end_date: @end_date,
      engineers: engineers,
    })
  end

  def self.engineers
    engineers = UsersFinder.group_engineers.merged_after(@start_date).merged_before(@end_date).call
    engineers.nodes.map do |user|
      merge_requests = user.authored_merge_requests.nodes

      mttms = merge_requests.map do |merge_request|
        if [merge_request.merged_at, merge_request.prepared_at].any?(&:nil?)
          puts 'Merge request has no merged_at or prepared_at'
          next
        end
        (Date.parse(merge_request.merged_at) - Date.parse(merge_request.prepared_at)).to_i
      end.compact

      activities = ContributionEventsFinder.call(user: user.username, before: @end_date, after: @start_date)

      OpenStruct.new({
        username: user.username,
        merge_requests: merge_requests,
        mttm: (mttms.sum(0.0) / mttms.size).round(1),
        mr_rate: (merge_requests.size / ((@end_date - @start_date).to_i/30)).round(1),
        activities: activities
      })
    end
  end
end
