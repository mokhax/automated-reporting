class OKRReportTemplate
  def self.render(context)
    @context = context

    puts "## #{ Config::GROUP_NAME }"
    puts ''

    puts '### Priorities'
    puts ''

    IssueTableTemplate.render(nodes: @context.issues)

    EpicTableTemplate.render(nodes: @context.epic_issues)
  end
end