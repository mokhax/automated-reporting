class EngineersReportTemplate
  extend ActionView::Helpers::DateHelper

  def self.render(context)
    time_ago = distance_of_time_in_words(context.start_date, context.end_date)

    puts  "# Engineering Report"
    puts  "## Merge requests merged from #{ context.start_date.strftime("%B %d") } to #{ context.end_date.strftime("%B %d") } (#{ time_ago })"
    puts '| Engineer | MR Rate | MTTM |'
    puts '| --- | --- | --- |'

    context.engineers.each do |engineer|
      puts "| #{ engineer.username } | #{ engineer.mr_rate } | #{ engineer.mttm } |"
    end

    context.engineers.each do |engineer|
      puts '<details>'
      puts "<summary> #{ engineer.username } - Show all merge requests </summary>"
      puts ''

      puts '| Title | Merged At |'
      puts '| --- | --- |'
      engineer.merge_requests.each do |merge_request|
        puts "| #{ merge_request.web_url }+ | #{ merge_request.merged_at } |"
      end

      puts '</details>'
      puts ''

      puts '<details>'
      puts "<summary> #{ engineer.username } - Show all contribution activity </summary>"
      puts ''

      puts '| Date | Contribution times |'
      puts '| --- | --- |'
      engineer.activities.each do |activity_date, activity|
        puts "#{ activity_date } #{ Date.parse(activity_date).strftime("%A") } | #{ activity.reverse.map {|a| "#{DateTime.parse(a['created_at']).strftime('%H:%M:%S')} - #{a['action_name']} #{a['target_title']&.truncate(20)}" }.join(' <br /> ') }"
      end

      puts '</details>'
      puts ''
    end
  end
end
