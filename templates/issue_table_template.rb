class IssueTableTemplate
  def self.render(nodes:)
    return if nodes.empty?

    puts '| Priority | Current Status | Progress | Milestone | Due Date | DRI |'
    puts '| --- | --- | --- | --- | --- | --- |'

    nodes.each do |issue|
      puts row(issue)
    end
    puts ''
  end

  private

  def self.row(issue)
    template_row = <<~MARKDOWN
    | #{ issue.web_url }+ | #{ [issue.health_status, issue.dri_comment].compact.join(' <br /> ') } | #{ issue.state } <br /> #{ issue.labels(/workflow|priority|severity/).join(' <br /> ') } | #{ issue.milestone&.title || '-' } | #{ issue.due_date || '-' } | #{ issue.assignees } |
    MARKDOWN
  end
end