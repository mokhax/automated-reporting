class EpicTableTemplate
  def self.render(nodes:)
    return if nodes.empty?

    nodes.each do |epic|
      puts '| Priority | Current Status | Progress | Milestone | Due Date | DRI |'
      puts '| --- | --- | --- | --- | --- | --- |'
      puts "| :sparkles: :sparkles: **#{ epic.web_url }+** | #{ epic.dri_comment } | **#{ epic.progress }** | - | #{ epic.due_date } | - |"

      puts '<details>'
      puts "<summary> Show issue details for #{ epic.web_url }+ </summary>"
      puts ''

      IssueTableTemplate.render(nodes: epic.issues)

      puts '</details>'
      puts ''
    end
  end
end