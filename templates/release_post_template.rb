class ReleasePostTemplate
  def self.render(nodes:)
    return if nodes.empty?

    puts '| Priority | Milestone | Epic | Labels | Closed At |'
    puts '| --- | --- | --- | --- | --- |'

    nodes.each do |issue|
      puts row(issue)
    end
    puts ''
  end

  private

  def self.row(issue)
    template_row = <<~MARKDOWN
    | #{ issue.web_url }+ | #{ issue.milestone&.title || '-' } | #{ issue.epic&.title || '-' } | #{ issue.all_labels } | #{ issue.closed_at || '-' } |
    MARKDOWN
  end
end