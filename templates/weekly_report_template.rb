class WeeklyReportTemplate
  def self.render(context)
    @context = context

    puts "## #{ Config::GROUP_NAME }"
    puts ''

    puts '### Priorities'
    puts ''

    IssueTableTemplate.render(nodes: @context.open_issues)

    puts '<details>'
    puts "<summary> Show all #{ @context.closed_issues.count } issues not in epics that closed this week </summary>"
    puts ''

    IssueTableTemplate.render(nodes: @context.closed_issues)

    puts '</details>'
    puts ''

    EpicTableTemplate.render(nodes: @context.epic_issues)

    error_budget = <<~MARKDOWN
    ### Error Budget, Security, & Infradev
    Error Budget - [dashboard](https://dashboards.gitlab.net/d/product-govern_error_budget/product-error-budgets-govern?orgId=1), [issue board](https://gitlab.com/groups/gitlab-org/-/boards/5557640?label_name[]=#{Config::GROUP_LABEL})


    1. Error Budget
      - ...% (28-day)
      - Supporting information if under threshold (normally 99.95%)

    MARKDOWN

    puts error_budget

    puts '#### Security Issues'
    puts ''
    if @context.security_issues.empty?
      puts 'None'
    else
      IssueTableTemplate.render(nodes: @context.security_issues)
    end

    puts '#### Infradev Issues'
    puts ''

    if @context.infradev_issues.empty?
      puts 'None'
    else
      IssueTableTemplate.render(nodes: @context.infradev_issues)
    end
  end
end