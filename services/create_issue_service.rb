class CreateIssueService
  def self.call(title:, description:, project_path: Config::GROUP_PROJECT_PATH,  epic_id: nil)
    args = {
      projectPath: project_path,
      title: title,
      description: description,
      epicId: epic_id
    }

    result = GraphQL::Mutations::CreateIssueMutation.new(variables: args).results

    puts result.to_h
  end
end