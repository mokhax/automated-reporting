module IssuableScopes
  def for_group
    scope_variables[:groupLabelName] = Config::GROUP_LABEL

    return self
  end

  def auto_reporting
    scope_variables[:labelNames].push(Config::AUTO_REPORT_LABEL)

    return self
  end

  def okr
    scope_variables[:labelNames].push(Config::OKR_LABEL)

    return self
  end

  def planning
    scope_variables[:labelNames].push(Config::PLANNING_LABEL)

    return self
  end

  def security
    scope_variables[:labelNames].push(Config::SECURITY_LABEL)

    return self
  end

  def infradev
    scope_variables[:labelNames].push(Config::INFRADEV_LABEL)

    return self
  end

  def ignore
    scope_variables[:labelNames].push(Config::IGNORE_LABEL)

    return self
  end

  def open
    scope_variables[:state] = Config::OPENED_STATE

    return self
  end

  def closed_after(date)
    scope_variables[:closedAfter] = date

    return self
  end

  def epic(epic_id)
    scope_variables[:epicId] = epic_id

    return self
  end

  def title(string)
    scope_variables[:search] = string
    scope_variables[:in] = ['TITLE']

    return self
  end

  def weekly_update
    epic(Config::WEEKLY_UPDATE_EPIC_ID).title(Config::WEEKLY_UPDATE_TITLE_SEARCH_STRING)
  end

  def release_post
    title(Config::RELEASE_POST_REPORT_TITLE_SEARCH_STRING)
  end
end