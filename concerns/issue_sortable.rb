module IssueSortable
  WORKFLOW_SORTING = {
    "workflow::in review": 1,
    "workflow::in dev": 2,
    "workflow::ready for development": 3,
    "workflow::refinement": 4,
    "workflow::problem validation": 5
  }

  HEALTH_SORTING = {
    'atRisk': 10,
    'needsAttention': 20
  }

  def sort_by_workflow_and_state(unsorted)
    unsorted.sort_by do |u|
      state_priority = u.state == 'closed' ? 200 : 100

      health_priority = HEALTH_SORTING[u.object.health_status.to_s.to_sym] || 30

      workflow_label = u.object.labels.nodes.map(&:title).find {|label| label =~ /workflow/ }&.to_sym
      workflow_priority = WORKFLOW_SORTING[workflow_label] || 6

      state_priority + health_priority + workflow_priority
    end
  end
end