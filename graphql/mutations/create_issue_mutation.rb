module GraphQL
  module Mutations
    class CreateIssueMutation < GraphQL::BaseClient
      QUERY = GraphQL::BaseClient::Client.parse <<-'GRAPHQL'
        mutation($description: String!, $title: String!, $projectPath: ID!, $epicId: EpicID) {
          createIssue(input: { description: $description, title: $title, projectPath: $projectPath, epicId: $epicId }) {
            errors
            issue {
              webUrl
            }
          }
        }
      GRAPHQL
    end
  end
end