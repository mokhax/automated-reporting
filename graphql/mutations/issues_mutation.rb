module GraphQL
  module Mutations
    class IssuesMutation < GraphQL::BaseClient
      QUERY = GraphQL::BaseClient::Client.parse <<-'GRAPHQL'
        mutation($ids: [IssueID!]!, $parentId: IssueParentID!, $stateEvent: IssueStateEvent) {
          issuesBulkUpdate(input: { ids: $ids, parentId: $parentId, stateEvent: $stateEvent }) {
            errors
            updatedIssueCount
          }
        }
      GRAPHQL
    end
  end
end