module GraphQL
  module Queries
    class IssuesQuery < GraphQL::BaseClient
      include IssueSortable

      QUERY = GraphQL::BaseClient::Client.parse <<-'GRAPHQL'
        query ($state:	IssuableState, $groupLabelName: [String], $labelNames: [String!], $closedAfter: Time, $epicId: String, $search: String, $in: [IssuableSearchableField!]) {
          issues(state: $state, closedAfter: $closedAfter, labelName: $groupLabelName, or: { labelNames: $labelNames }, epicId: $epicId, search: $search, in: $in) {
            nodes {
              id
              createdAt
              closedAt
              webUrl
              state
              healthStatus
              epic {
                title
              }
              milestone {
                title
              }
              dueDate
              assignees {
                nodes {
                  username
                }
              }
              labels {
                nodes {
                  title
                }
              }
              discussions(last: 10) {
                nodes {
                  notes(last: 10) {
                    nodes {
                      awardEmoji {
                        nodes {
                          name
                        }
                      }
                      author {
                        username
                      }
                      body
                      createdAt
                    }
                  }
                }
              }
            }
          }
        }
      GRAPHQL

      def nodes
        nodes = results.data.issues.nodes.map do |issue|
          IssuePresenter.new(object: issue)
        end
        sort_by_workflow_and_state(nodes)
      end
    end
  end
end