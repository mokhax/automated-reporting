module GraphQL
  module Queries
    class ProjectQuery < GraphQL::BaseClient
      QUERY = GraphQL::BaseClient::Client.parse <<-'GRAPHQL'
        query ($path:	ID!, $date: Time, $filePath: [String!]!, $branchName: String) {
          project(fullPath: $path) {
            id,
            milestones(includeAncestors: true, state: active, containingDate: $date) {
              nodes {
                title
                startDate
              }
            },
            repository {
              blobs(ref:$branchName, paths: $filePath) {
                nodes {
                  rawBlob
                }
              }
            }
          }
        }
      GRAPHQL

      def nodes
        results.data.project
      end
    end
  end
end