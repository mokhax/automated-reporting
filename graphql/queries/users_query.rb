module GraphQL
  module Queries
    class UsersQuery < GraphQL::BaseClient
      QUERY = GraphQL::BaseClient::Client.parse <<-'GRAPHQL'
        query ($usernames: [String!], $createdAfter: Time, $createdBefore: Time, $state: MergeRequestState, $mergedAfter: Time, $mergedBefore: Time, $sort: MergeRequestSort) {
          users(usernames: $usernames) {
            nodes {
              username,
              authoredMergeRequests(createdAfter: $createdAfter, createdBefore: $createdBefore, state: $state, mergedAfter: $mergedAfter, mergedBefore: $mergedBefore, sort: $sort) {
                nodes {
                  title
                  webUrl
                  totalTimeSpent
                  state
                  preparedAt
                  mergedAt
                }
              }
            }
          }
        }
      GRAPHQL

      def nodes
        results.data.users
      end
    end
  end
end
