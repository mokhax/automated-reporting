module GraphQL
  module Queries
    class EpicIssuesQuery < GraphQL::BaseClient
      # TODO: This is just a generic "Open" epic list in the gitlab-org group
      OPEN_BOARD_LIST_GID = "gid://gitlab/Boards::EpicList/1132984"

      QUERY = GraphQL::BaseClient::Client.parse <<-GRAPHQL
        query ($labelNames: [String]) {
          epicBoardList(id: "#{ OPEN_BOARD_LIST_GID }") {
            epics(filters: { labelName: $labelNames }) {
              nodes {
                webUrl
                healthStatus
                dueDate
                issues {
                  nodes {
                    healthStatus
                    webUrl
                    state
                    milestone {
                      title
                    }
                    dueDate
                    assignees {
                      nodes {
                        username
                      }
                    }
                    labels {
                      nodes {
                        title
                      }
                    }
                    discussions(last: 10) {
                      nodes {
                        notes(last: 10) {
                          nodes {
                            awardEmoji {
                              nodes {
                                name
                              }
                            }
                            author {
                              username
                            }
                            body
                            createdAt
                          }
                        }
                      }
                    }
                  }
                }
                discussions(last: 10) {
                  nodes {
                    notes(last: 10) {
                      nodes {
                        awardEmoji {
                          nodes {
                            name
                          }
                        }
                        author {
                          username
                        }
                        body
                        createdAt
                      }
                    }
                  }
                }
              }
            }
          }
        }
      GRAPHQL

      def nodes
        epics = results.data.epic_board_list.epics.nodes

        epics.map do |epic|
          EpicPresenter.new(object: epic)
        end
      end
    end
  end
end