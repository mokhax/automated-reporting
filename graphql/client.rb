require "graphql/client"
require "graphql/client/http"

module GraphQL
  class BaseClient
    ## FIXME - this connects everytime the class is required
    HTTP = GraphQL::Client::HTTP.new("https://gitlab.com/api/graphql") do
      def headers(context)
        { "Authorization": "Bearer #{Config::GITLAB_API_PRIVATE_TOKEN}" }
      end
    end

    Schema = GraphQL::Client.load_schema(HTTP)

    Client = GraphQL::Client.new(schema: Schema, execute: HTTP)

    attr_accessor :variables

    def initialize(variables: {})
      @variables = variables
    end

    def results
      Client.query(self.class::QUERY, variables: self.variables)
    end
  end
end